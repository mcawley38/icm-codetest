<?php

namespace App\Console\Commands;

use App\Classes\Game;
use App\Classes\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class Snap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snap:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $player1;
    private $player2;
    private $game;

    /**
     * Create a new command instance.
     *
     * @param User $player1
     * @param User $player2
     * @param ConsoleOutput $output
     */
    public function __construct(User $player1, User $player2, ConsoleOutput $output)
    {
        parent::__construct();
        $this->player1 = $player1;
        $this->player2 = $player2;
        $this->output = $output;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $match_type = $this->ask('What type of matching would you like (suits|faces|suits and faces)?');
        $deck_count = $this->ask('How many decks do you wish to use?');

        $sanitised_match_type = is_string($match_type) ? $match_type : 'suits and faces';
        $sanitised_deck_count = is_numeric($deck_count) ? $deck_count: 1;

        $this->game = new Game($this->player1, $this->player2, $sanitised_match_type, $sanitised_deck_count);
        unset($this->game);
    }
}
