<?php
/**
 * Created by PhpStorm.
 * User: Myles
 * Date: 29/06/2020
 * Time: 16:05
 */

namespace App\Classes;


use Symfony\Component\Console\Output\ConsoleOutput;

class Game
{
    private $card_pool = [];
    private $players = [];
    private $deck_count;
    private $game_ended = false;
    private $match_type;

    /**
     * @param User $player1
     * @param User $player2
     * @param string $match_type
     * @param int $deck_count
     */
    public function __construct(User $player1, User $player2, string $match_type, int $deck_count)
    {
        $this->players = [
            $player1,
            $player2,
        ];
        $this->deck_count = $deck_count ? $deck_count : 1;
        $this->match_type = $match_type;

        $this->outputLine('Game initialised!');
        $this->initialiseDeck();
        $this->runGame();
    }

    private function initialiseDeck(): void
    {
        $cards = [];

        for ($i = 1; $i <= $this->deck_count; $i++) {
            foreach (config('game.cards.suits') as $suit) {
                foreach (config('game.cards.values') as $value) {
                    $cards[] = [
                        'value' => $value,
                        'suit' => $suit
                    ];
                }
            }
        }

        shuffle($cards);

        $len = count($cards);

        $this->players[0]->setCards(array_slice($cards, 0, $len / 2));
        $this->players[1]->setCards(array_slice($cards, $len / 2));

        $this->outputLine('Deck initialised!');
    }

    private function runGame(): void
    {
        $this->outputLine('Game started!');

        while (!$this->game_ended) {
            $this->roundLogic();
        }
    }

    private function endGame(): void
    {
        $this->game_ended = true;

        $win_msg = !$this->players[0]->hasCards()
            ? ( !$this->players[1]->hasCards()
                ? 'The game is a draw!'
                : 'Player 1 won!')
            : 'Player 2 won!';

        $this->outputLine($win_msg);
    }

    private function roundLogic(): void
    {
        $this->outputLine('Round started!');

        $cards = [];

        foreach ($this->players as $index => $player) {
            if (!($cards[] = $player->playCard())) {
                $this->endGame();

                break;
            }

            $this->card_pool = array_merge($this->card_pool, $cards);

            $current_card = end($cards);

            $this->outputLine('Player' . ($index + 1) . ': has played ' . $current_card['value'] . ' of ' . end($cards)['suit']);
        }

        count($cards) === count($this->players) && $this->isMatch($cards) && $this->awardCards();
    }

    private function isMatch(array $round_cards): bool
    {
        switch ($this->match_type) {
            case 'suits':
                return ($round_cards[0]['suit'] === $round_cards[1]['suit']);
                break;
            case 'face':
                return ($round_cards[0]['value'] === $round_cards[1]['value']);
                break;
            default:
                return ($round_cards[0]['suit'] === $round_cards[1]['suit'] && $round_cards[0]['value'] === $round_cards[1]['value']);
        }
    }

    private function awardCards(): void
    {
        $rand = rand(0, 1);
        $this->outputLine('player' . $rand . ' has won the snap');
        $this->players[$rand]->awardCards($this->card_pool);
        $this->card_pool = [];
    }

    private function outputLine(string $msg): void
    {
        (new ConsoleOutput())->writeln($msg);
    }
}
