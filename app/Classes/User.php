<?php
/**
 * Created by PhpStorm.
 * User: Myles
 * Date: 29/06/2020
 * Time: 16:04
 */

namespace App\Classes;


class User
{
    private $cards = [];

    public function playCard(): ?array
    {
        return array_shift($this->cards);
    }

    public function hasCards(): bool
    {
        return !!count($this->cards);
    }

    public function awardCards(array $cards): void
    {
        $this->cards = array_merge($this->cards, $cards);
    }

    public function setCards(array $cards): void
    {
        $this->cards = $cards;
    }
}
